#ifndef SCHEDULER
#define SCHEDULER

#include <sstream>
#include <vector>
#include <queue>
#include <iostream>
#include <string>
#include <algorithm>
#include <fstream>
#include <sstream>
#include "process.h"

/*
 * Class: Multi-Level Feedback Queues
 * Description:
 *  1 - 5 queues that use Aging, and Demotion as an indicator for what queue to be in.
 *  When a process is young, it doesnt need priority. If it gets the processor, and does not complete, It gets Demoted to a less important state.
 *  
 */
class MFQS{
public:
    // Initializer for The Queues. Called on Startup.
    MFQS(int NumQueues, int TimeQuant, std::vector<process> Input, int ageLen);

    //Edits the process information 0 = done, 1 = demote
    int CPU( process &current, int TQ);

    //executes the things that need to be done this step in time.
    void Step();

    /* looks for any processes that will age in the next time quantom or burst (the shorter)
     * Returns an int (0 = no PIDs were found, 1 = aging PIDs were found)
     * Any aging PIDs are pushed into the queue passed by parameter
     * processes are sorted by queue, and should retain their order
     */
    int checkForAging(std::vector< std::queue<process>> &agingPIDs);

    /* checks for ties between current demoted PID and any aged PIDs
     * PIDs should retain their order
     * insert demoted queue into the aging queue (based on time)
     * resolve the tie, if there is one
     */
    void checkTies(std::queue<process> &agingPIDs, process &demotedPID, int QNum);
    
    //puts the PID in the next process down (if there is a queue of lower level
    //the PID should be poped from the previous queue and sent to demote
    void Demote(process &process, int currQNum);
    
    void displayStats();

    void VectorPrint(std::vector<process> vect);
    void QueuePrint(std::queue<process> queue);
    void MFQSPrint();

    static bool compareArrival(process a, process b);
    static bool comparePriority(process a, process b);
    static bool comparePIDs(process a, process b);

    void Shutdown();
private:
    int NumQueues;
    int TimeQuant;
    int ageLen;
    int time;
    
    // these are processes that are sorted by arrival time.
    std::queue<process> Input;

    // these are finished processes.
    std::vector<process> Output;

    // these are the multi level Queues. [] will be 2 - 5, typically. 
    std::vector< std::queue<process> > Multi_Queue;
};

/*
 * Class: Real Time Scheduler
 * Description:
 * This scheduler only runs based on Deadlines, and the arrival time. 
 * Its a single vector, that is going to be craaazy.
 */
class RTS{
public:
    // Constructor
    RTS(int TQ, std::vector<process> Input, bool isHard);
    
    // Does the CPU editing
    process CPU(process bob);
    
    // Takes a single step in time.
    void Step();
    
    //checks to see if B is greater than A.
    static bool compareRTS(process A, process B);
    
    static bool compareRTSArrival(process A, process B);
    
    // Print
    void Print();
    
    //Is Hard?
    bool isHard();
    
    bool isEmpty();
    
private:
    std::vector<process> Input_list;
    std::vector<process> Process_list;
    std::vector<process> Output_list;
    std::vector<process> Dead_list;
    unsigned long elapsed_Time;
    process current;
    process new_pro;
    int TQ;
    bool Hard;
    bool Input_Empty;
    bool Process_Empty;    
};


/*
 * Class: Windows Hybrid Scheduler
 * Description:
 * A dynamic scheduler that works off a determined priority number.
 * 
 */

class WHS{
public:
    WHS(std::vector<process> Input, int TQ);
    process CPU(process bob);
    void PromoteAge();
    process PromoteIO(process current);
    process Demote(process bob, int TTQ);
    void Step();
    void Print();
    bool isEmpty();
    static bool compareWHS(process A, process B);
    static bool compareWHSArrival(process A, process B);
private:
    std::vector<process> Input_list;
    std::vector<process> Process_list;
    std::vector<process> Output_list;
    std::vector<process> IO_list;
    unsigned long elapsed_Time;
    process current;
    process new_pro;
    int TQ;
    int Agefactor;
    int AgeIncrement;
    bool Input_Empty;
    bool Process_Empty; 
};

#endif
