#include <vector>
#include <queue>
#include "process.h"
#include "schedule.h"

#ifndef GANT
#define GANT
#endif

/*
#ifndef DEBUG 
#define DEBUG 
#endif
*/
/*
#ifndef V 
#define V 
#endif
*/

using std::cout;
using std::endl;
using std::vector;
using std::queue;

MFQS::MFQS(int NumQueues, int TimeQuant, vector<process> Input, int ageLen) {
	cout << "Setting up..." << endl;
	
	std::sort(Input.begin(), Input.end(), compareArrival);
	unsigned int size = Input.size();
	for(unsigned int i = 0; i < size; i++) {
		this->MFQS::Input.push(Input.at(i));
		//cout << "[" << MFQS::Input.back().getPid() << "] A: " << MFQS::Input.back().getArrival() << " P: " << MFQS::Input.back().getPriority() << endl;
	}

	this->NumQueues = NumQueues;
	for(int i = 0; i < MFQS::NumQueues; i++) {
		this->Multi_Queue.push_back(queue<process>());
	}

	this->TimeQuant = TimeQuant;
	this->ageLen = ageLen;
	this->time = 0;
	cout << "Starting MFQS... " << endl;
}

int MFQS::CPU(process &current, int TQ) {
	if(current.getStart() < 0) {
		current.setStart(time);
	}

	if(current.getBurstRem() > TQ) { //greater than time quantom, demote
		current.setBurstRem(current.getBurstRem() - TQ);
		time += TQ;
		return 1;
	} else { //less than or equal to the Time Quantom
		current.setEnd(time + current.getBurstRem());
		time += current.getBurstRem(); 
		current.setBurstRem(0);
		return 0;
	}
}

void MFQS::Step() {
	bool Done = false;
	while(!Done) {
		//loop through the input queue (queue already has resolved ties)
		//find all PIDs that will start within this next time quantum
	
		#ifdef DEBUG 
		cout << "Time: " << MFQS::time << endl; 
		#endif
		bool shouldIStop = false;
		queue<process> inputs;
		while(!shouldIStop && !Input.empty()) {
			if(Input.front().getArrival() >= time
			&& Input.front().getArrival() <= time + TimeQuant) {
				inputs.push(Input.front());
				Input.pop();
			} else { 
				shouldIStop = true;
			}
		}
		
		while(!inputs.empty()) {
			inputs.front().setQ(0);
			inputs.front().setStartInQueue(time);
			Multi_Queue[0].push(inputs.front());
			inputs.pop();
		}
	
		int QNum = 0;
		bool processToRun = false;
		#ifdef V
		cout << "MQ[0] size: " << Multi_Queue[0].size() << endl;
		MFQS::MFQSPrint();
		#endif


		//find the first queue with a process, and run it
		while(!processToRun && QNum < NumQueues) {
			if(!Multi_Queue[QNum].empty()) {
				processToRun = true; //found a process
	
				//check for aging PIDs
				vector< queue<process> > agingPIDs;
				for (int i = 0; i < MFQS::NumQueues; i++) {
					agingPIDs.push_back(queue<process>());
				}
				int agingResult = checkForAging(agingPIDs);
				bool runCPU = true;	
				if(Multi_Queue[QNum].empty()) { //if the aging process was the last one in the queue, decrement QNum, and grab it on the queue above
					QNum--;
					runCPU = false;
				}
				#ifdef V							
				cout << "Aging res: " << agingResult << endl;
				#endif
				int cpuResult;
				process currPID;	
				if(runCPU) {
					//send the PID to the "cpu"
					currPID = Multi_Queue[QNum].front();
					Multi_Queue[QNum].pop();
					cpuResult = MFQS::CPU(currPID, (TimeQuant * (QNum + 1)));
					#ifdef V
					cout << "CPU res: " << cpuResult << endl;	
					#endif
				}
				if(cpuResult > 0 && runCPU) { //demote the PID
					#ifdef DEBUG
					cout << "process will be demoted." << endl;
					#endif
					if(agingResult == 0) {
						#ifdef DEBUG
						cout << "no aging needed" << endl;
						#endif
						Demote(currPID, QNum);
					} else {
						if(QNum + 2 < NumQueues && !agingPIDs[QNum + 2].empty()) {
							#ifdef DEBUG
							cout << "checking ties." << endl;
							QueuePrint(agingPIDs[QNum + 2]);
							#endif
							MFQS::checkTies(agingPIDs[QNum + 2], currPID, QNum + 1);
						
						} else {
							#ifdef DEBUG
							cout << "no conflicting aging" << endl;
							#endif
							currPID.setQ(QNum + 1);
							currPID.setStartInQueue(time);
							Demote(currPID, QNum);
						}
					}
	
				
					for(int i = 2; i < NumQueues; i++) {//Q0, and Q1 don't age
						//go through aging PIDs and age any left over, ignoring any delt with above
						if(i != QNum + 2) {
							#ifdef V
							cout << "aging the rest" << endl;
							//insert into queue[i] with agingPIDs[i]
							cout << "agingPIDs[i].empty(): " << agingPIDs[i].empty() << endl;
							#endif
							while(!agingPIDs[i].empty()) {
								#ifdef DEBUG
								cout << "found PIDs to age" << endl;
								cout << "agingPIDs[" << i << "]: ";
								QueuePrint(agingPIDs[i]);
								#endif
								
								agingPIDs[i].front().setQ(i-1);
								agingPIDs[i].front().setStartInQueue(time);
								Multi_Queue[i-1].push(agingPIDs[i].front());
								agingPIDs[i].pop();
							}
						}
					}
	
	
				} else { //put the PID in the done Queue
					//put into the done queue
					if(runCPU) {
						Output.push_back(currPID);
						#ifdef DEBUG
						cout << "PID done." << endl;
						#endif
					}
					for(int i = 2; i < NumQueues; i++) {//Q0, and Q1 don't age
						//go through and age any PIDs  
						//insert into queue[i] with agingPIDs[i]
						#ifdef V
						cout << "aging any PIDs." << endl;
						cout << "agingPIDs[i].empty(): " << agingPIDs[i].empty() << endl;
						#endif
							while(!agingPIDs[i].empty()) {
								#ifdef DEBUG
								cout << "found PIDs to age" << endl;
								cout << "agingPIDs[" << i << "]: ";
								QueuePrint(agingPIDs[i]);
								#endif

								agingPIDs[i].front().setQ(i-1);
								agingPIDs[i].front().setStartInQueue(time);
								Multi_Queue[i-1].push(agingPIDs[i].front());
								agingPIDs[i].pop();
							}
					}
				}
				#ifdef DEBUG
				cout << "done running PID(s). grab the next bunch" << endl << endl;
				#endif
			} else {
				if(QNum == MFQS::NumQueues - 1) {
					Done = true;
				}

				QNum++;
			}
		}
	}

	MFQS::displayStats();
}


int MFQS::checkForAging(vector< queue<process> > &agingPIDs) {
	//ignore the first two queues, since Q1 can't age to Q0, and Q0 can't age
	int res = 0;
	
	for(int i = 2; i < MFQS::NumQueues; i++) {
	//	cout << "checkAging() Multi_queue[i].empty(): " << MFQS::Multi_Queue[i].empty() << endl;
	//	cout << "checkAging() agingPIDs[i].empty(): " << agingPIDs[i].empty() << endl;

		bool amIDone = false;
		while(!amIDone && !MFQS::Multi_Queue[i].empty()) {
			#ifdef DEBUG
			cout << "adding PIDs to agingPIDs." << endl;
			#endif
			amIDone = true;

			if ((MFQS::time - MFQS::Multi_Queue[i].front().getStartInQueue()) >= MFQS::ageLen) {
				agingPIDs[i].push(MFQS::Multi_Queue[i].front());
				#ifdef DEBUG
				cout << "PID added: [" << agingPIDs[i].back().getPid() << "]" << endl;
				#endif
				MFQS::Multi_Queue[i].pop();
				res = 1;
				amIDone = false;
			}
		}
	}

	return res;
}

void MFQS::Demote(process &process, int currQNum) {
	if(currQNum + 1 >= NumQueues) { //can't be demoted
		#ifdef DEBUG
		cout << "can't Demote.  In last queue." << endl;
		#endif
		Multi_Queue[currQNum].push(process);	
	} else {
		#ifdef DEBUG
		cout << "Demote() begin" << endl;
		#endif
		process.setQ(currQNum + 1);
		process.setStartInQueue(time);
		Multi_Queue[currQNum + 1].push(process);
		#ifdef DEBUG 
		cout << "demoted." << endl;
		#endif

	}
}

void MFQS::checkTies(queue<process> &agingPIDs, process &demotedPID, int QNum) {

	while(!agingPIDs.empty()) {
	//	cout << "checkTies() agingPIDs.empty(): " << agingPIDs.empty() << endl;
		if(demotedPID.getArrival() < agingPIDs.front().getArrival()) {
			demotedPID.setStartInQueue(time);
			demotedPID.setQ(QNum);
			agingPIDs.front().setQ(QNum);
			agingPIDs.front().setStartInQueue(time);
			Multi_Queue[QNum].push(demotedPID);
			Multi_Queue[QNum].push(agingPIDs.front());
			agingPIDs.pop();
		} else if (demotedPID.getArrival() > agingPIDs.front().getArrival()) {
			demotedPID.setQ(QNum);
			demotedPID.setStartInQueue(time);
			agingPIDs.front().setStartInQueue(time);
			agingPIDs.front().setQ(QNum);
			Multi_Queue[QNum].push(agingPIDs.front());
			agingPIDs.pop();
			Multi_Queue[QNum].push(demotedPID);	
		} else { //tie
			if(demotedPID.getPriority() < agingPIDs.front().getPriority()) {
				demotedPID.setQ(QNum);
				demotedPID.setStartInQueue(time);
				agingPIDs.front().setStartInQueue(time);
				agingPIDs.front().setQ(QNum);
				Multi_Queue[QNum].push(demotedPID);
				Multi_Queue[QNum].push(agingPIDs.front());
				agingPIDs.pop();
			} else if (demotedPID.getPriority() > agingPIDs.front().getPriority()) {
				demotedPID.setQ(QNum);
				demotedPID.setStartInQueue(time);
				agingPIDs.front().setQ(QNum);	
				agingPIDs.front().setStartInQueue(time);
				Multi_Queue[QNum].push(agingPIDs.front());
				agingPIDs.pop();
				Multi_Queue[QNum].push(demotedPID);	
			} else {//still a tie
				if(demotedPID.getPid() < agingPIDs.front().getPid()) {
					demotedPID.setQ(QNum);
					demotedPID.setStartInQueue(time);
					agingPIDs.front().setQ(QNum);	
					agingPIDs.front().setStartInQueue(time);
					Multi_Queue[QNum].push(demotedPID);
					Multi_Queue[QNum].push(agingPIDs.front());
					agingPIDs.pop();
				} else {
					demotedPID.setQ(QNum);
					demotedPID.setStartInQueue(time);
					agingPIDs.front().setQ(QNum);	
					agingPIDs.front().setStartInQueue(time);
					Multi_Queue[QNum].push(agingPIDs.front());
					agingPIDs.pop();
					Multi_Queue[QNum].push(demotedPID);
				}
			}
		}
	}
}

bool MFQS::compareArrival(process a, process b) {
	if(a.getArrival() == b.getArrival()) {
		if(a.getPriority() == b.getPriority()) {
			return a.getPid() < b.getPid();
		} else {
			return a.getPriority() < b.getPriority();
		}
	} else {
		return a.getArrival() < b.getArrival();	
	}
}

bool MFQS::comparePriority(process a, process b) {
	return a.getPriority() < b.getPriority();
}

bool MFQS::comparePIDs(process a, process b) {
	return a.getPid() < b.getPid();
}

void MFQS::displayStats() {
	long AWT = 0;
	long ATT = 0;
	cout << "Gant Chart" << endl;
	cout << "MFQS::Output.size: " << MFQS::Output.size() << endl;
	long TotWaitTime = 0;
	long TotTurnTime = 0;
	for(unsigned int i = 0; i < MFQS::Output.size(); i++) {
		process pid = MFQS::Output.at(i);
		
		#ifdef GANT
		cout << "Pid: " << pid.getPid() << " Queue Ended in: " << pid.getQ() 
		<< " Start: " << pid.getStart() 
		<< " End: " << pid.getEnd() << endl;
		#endif
		TotWaitTime += pid.getStart() - pid.getArrival();
		TotTurnTime += pid.getEnd() - pid.getStart();
	}
	
	AWT = TotWaitTime / MFQS::Output.size();
	ATT = TotTurnTime / MFQS::Output.size();
	cout << endl << "----------------------------" << endl;
	cout << "Average Wait Time: " << AWT << endl;
	cout << "Average Turnaround Time: " << ATT << endl;
	cout << "Total Number of Processes Scheduled: " << MFQS::Output.size() << endl;

		
}

void MFQS::MFQSPrint() {
	#ifdef V
		cout << "Input Q: " << endl;
		QueuePrint(MFQS::Input);
		cout << endl << endl;

		
		cout << "MQ: " << endl;
		cout << "MQ[0]: "; 
		QueuePrint(MFQS::Multi_Queue[0]);
		cout << endl << "MQ[1]: ";
		QueuePrint(MFQS::Multi_Queue[1]);
		cout << endl << "MQ[2]: ";
		QueuePrint(MFQS::Multi_Queue[2]);
		cout << endl << "MQ[3]: ";
		QueuePrint(MFQS::Multi_Queue[3]);
		cout << endl << "MQ[4]: ";
		QueuePrint(MFQS::Multi_Queue[4]);
		cout << endl << endl;

		cout << "Output Q: " << endl;
		VectorPrint(MFQS::Output);
		cout << endl << endl;
	#endif
}

void MFQS::QueuePrint(queue<process> queue) {
	while(!queue.empty()) {
		process process = queue.front();
		queue.pop();
		
		cout << "[" << process.getPid() << "]: A: "
		<< process.getArrival() << " BR: " << process.getBurstRem()
		<< " P: " << process.getPriority() << endl;
	}
}

void MFQS::VectorPrint(vector<process> vect) {
	for(unsigned int i = 0; i < vect.size(); i++) {
		cout << "[" << vect.at(i).getPid() << "]: A: "
		<< vect.at(i).getArrival() << " BR: " << vect.at(i).getBurstRem()
		<< " P: " << vect.at(i).getPriority() << endl;
	}
}

