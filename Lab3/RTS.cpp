#include "schedule.h"

//Constructor
RTS::RTS(int TQ, std::vector<process> Input, bool isHard){
    this->Input_list = Input;

    std::sort(Input_list.begin(),Input_list.end(),compareRTSArrival);
    Process_list.clear();
    Output_list.clear();
    this->elapsed_Time = 0;
    this->TQ = TQ;   
    this->Hard = isHard;
    this->Input_Empty = false;
    this->Process_Empty = false;
    this->current = process();
    this->new_pro = process();
#ifdef DEBUG
        std::cout << "At Initialization... ";
        std::cout <<" Process List Size: " << Process_list.size();
        std::cout <<" Input_list Size: " << Input_list.size();
        std::cout <<" Output_List Size: " << Output_list.size() << std::endl; 
#endif
}

//Does the CPU editing to Bob.
process RTS::CPU (process bob){
#ifdef DEBUG_2
    std::cout << "Bob REM: " << bob.getBurstRem() << std::endl;
    std::cout << "CTQ Process Size: " << Process_list.size() << std::endl;
    std::cout << "CTQ Input Size: " << Input_list.size() << std::endl;
    std::cout << "CTQ Out Size: " << Output_list.size() << std::endl;
#endif
    if(bob.getStart() == -1)
        bob.setStart(elapsed_Time);
    bob.setBurstRem( bob.getBurstRem() - 1);

#ifdef DEBUG
    bob.print();
#endif
    return bob;

}

/* 
 * RTS::Step();
 * 
 * Description: This method moves time forward one step.
 * 
 * This involves (in no particular order):
 * 1. Checking the process that was just on the CPU vs time Quantum.
 * 2. Checking the current Input to see if any NEED the CPU.
 * 3. Killing any processes that did not make the deadline (IMPLEMENTED)
 * 4. Getting a process on the CPU.
 * 5. increment Time Elapsed. (IMPLEMENTED)
 * 6. Sort the vector, using comparator(IMPLEMENTED).
 */
void RTS::Step(){
    new_pro = process(1000000,100000,100000,100000,100000,100000);
    //gather new processes.
    if(!Input_Empty){
        while((unsigned int) Input_list.front().getArrival() == this->elapsed_Time && Input_list.size() > 0){
            //If front is better than current new best...      
            if(Input_list.front().getDeadline() < this->new_pro.getDeadline())
                new_pro = Input_list.front();
            Process_list.push_back(Input_list.front());
            Input_list.erase(Input_list.begin());
            if(Input_list.size() == 1){
                // We have the last input, So we need to make sure we stop Input here.
                new_pro = Input_list.front();
                Input_list.erase(Input_list.begin());
                Input_Empty = true;
            }
        }
    }else{
        //Provide a "New" Process.
         new_pro = process(1000000,100000,100000,100000,100000,100000);
    }
    
    if(Process_list.size() > 0){
        // check Pre-emption, Time Interrupt, Finishing, and current's Deadline.
        if(new_pro.getDeadline() < current.getDeadline() || elapsed_Time % TQ == 0 || current.getBurstRem() == 0 || current.getDeadline() < (int) elapsed_Time){
            // We need the newest process to go.
            if(current.getDeadline() < (int) elapsed_Time){
                //current is DEAD!
                current.setEnd(elapsed_Time);
                Dead_list.push_back(current);
            }else if(current.getBurstRem() == 0){
                //We've finished~!
                current.setEnd(elapsed_Time);
                Output_list.push_back(current);
            }else{
                //We must have been TQ'd or a better process opened up.
                Process_list.push_back(current);
            }
            //We've done all checks for current, On to work~!

            std::sort(Process_list.begin(),Process_list.end(),compareRTS);
            current = CPU(Process_list.front());
            Process_list.erase(Process_list.begin());

        }else{
            current = CPU(current);
        }
        //Run through the Process List, check all deadlines.
        for(unsigned int i = 0; i < Process_list.size(); i++){
            if((unsigned int) Process_list.at(i).getDeadline() < this->elapsed_Time || (unsigned int) Process_list.at(i).getBurstRem() > Process_list.at(i).getDeadline() - this->elapsed_Time){
                Dead_list.push_back(Process_list.at(i));
                Process_list.erase(Process_list.begin()+i);
            }
        }
    }
    this->elapsed_Time++;
    
#ifdef DEBUG_2
    if(this->elapsed_Time % TQ == 0){
        std::cout <<"Time Interrupt~!" << std::endl;
        std::cout <<" Process List Size: " << Process_list.size();
        std::cout <<" Input_list Size: " << Input_list.size();
        std::cout <<" Output_List Size: " << Output_list.size(); 
        std::cout <<" Dead: " << Dead_list.size() << std::endl; 
    }
#endif
}

void RTS::Print(){
    long AWT = 0;
    long ATT = 0;
    std::cout << "\n\n COMPLETED!" << std::endl;
#ifdef DEBUG
    std::cout << "\n\n\nPrinting Process_list: " << std::endl;
    for(unsigned int i = 0; i <  Process_list.size(); i++ ){
        Process_list.at(i).print();
    }
    std::cout << "\n\n\nPrinting Output_list: " << std::endl;
    for(unsigned int c = 0; c <  Output_list.size(); c++ ){
        Output_list.at(c).print();
    }
      
    std::cout << "\n\nPrinting Dead process List: " << std::endl;
    for(unsigned int i = 0; i <  Dead_list.size(); i++ ){
        Dead_list.at(i).print();
    }
#endif

    for(unsigned int i = 0; i < Output_list.size(); i++){
        process temp = Output_list[i];
        AWT += temp.getStart() - temp.getArrival();
        ATT += temp.getEnd() - temp.getStart();
    }    
AWT = AWT / Output_list.size();
ATT = ATT / Output_list.size();

std::cout << std::endl << "--------------------------" << std::endl;
std::cout << "Average Output Wait Time: "<< AWT << std::endl;
std::cout << "Average Output Turnaround Time: "<< ATT << std::endl;
std::cout << "Total Number of Processes Finished: "<< (Output_list.size()) << std::endl;

}

bool RTS::isHard(){
    return this->Hard;
}

bool RTS::isEmpty(){
    return Input_list.empty() && Process_list.empty();
}

//checks to see if B is greater than A.
bool RTS::compareRTS(process A, process B){
    if(( A.getDeadline()) == ( B.getDeadline() )){
        return A.getBurstRem() < B.getBurstRem();
    }else { 
        return ( A.getDeadline() ) < ( B.getDeadline() ); 
    }
}

bool RTS::compareRTSArrival(process A, process B){
    if(( A.getArrival()) == ( B.getArrival() )){
        return A.getBurstRem() < B.getBurstRem();
    }else { 
        return ( A.getArrival() ) < ( B.getArrival() ); 
    }
}

