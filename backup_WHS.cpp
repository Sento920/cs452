#include "schedule.h"
#define DEBUG


/*
 * Class: Windows Hybrid Scheduler
 * Description:
 * A dynamic scheduler that works off a determined priority number.
 * Priority is determined from read in (This is the base val, cannot go below that)
 *  - Increased through age, and length of IO. 
 *      - Age is a number of clock ticks since it last got the cpu.
 *      - IO is done at the second to last piece of its burst, unless process ends before that.
 *  - Decreased if it lasts more than 1 time quantom buy the length of that quantom.
 *  - There are 2 "Bands" of priority: those below 49, and those above it. 0 (highest) -> 100 (lowest) inclusive.
 * The bands will be maintained inside of Promote and Demote, since we will already have our hands in the guts. This should be pretty easy to maintain, since we have the "Concrete" Priority around to see which band it is in.
 */ 



WHS::WHS(std::vector<process> Input, int TTQ){
    this->Input_list = Input;
    std::sort(Input_list.begin(),Input_list.end(),compareWHSArrival);
    Input.front().print();
    this->Process_list.clear();
    this->Output_list.clear();
    this->TQ = TTQ;
    this->elapsed_Time = 0;
    this->Agefactor = 100;
    this->AgeIncrement = 10;
    
    #ifdef DEBUG
    std::cout << "At Initialization... ";
    std::cout <<" Process List Size: " << Process_list.size();
    std::cout <<" Input_list Size: " << Input_list.size();
    std::cout <<" Output_List Size: " << Output_list.size(); 
    std::cout <<" TQ: " << this->TQ << std::endl;
	#endif
}

/*CPU():
 * Does the editing for a single active process. 
 * Should be called Semi-recursively using Time Quantum reduction Concepts
 * Should be done After promotion, and before demotion.
 * Will probably be really complicated, as there is a lot of things to keep track of.
 */
void WHS::CPU(process bob, int TTQ){
#ifdef DEBUG
    std::cout << "Time: " << this->elapsed_Time << std::endl;
    std::cout << "TTQ: " << TTQ << std::endl;
    std::cout << "Bob REM: " << bob.getBurstRem() << std::endl;
    std::cout << "Bob REM - TTQ: " << (bob.getBurstRem() - TTQ) << std::endl;
    std::cout << "Bob Dynamic Priority: " << bob.getDynPriority() << std::endl;
    std::cout << "Bob Static Priority: " << bob.getPriority() << std::endl;
    std::cout << "CTQ Process Size: " << Process_list.size() << std::endl;
    std::cout << "CTQ Input Size: " << Input_list.size() << std::endl;
    std::cout << "CTQ Out Size: " << Output_list.size() << std::endl;
    bob.print();
#endif 
    if(bob.getIO() > 0){
        //Bob must do IO if bob has it, on the second to last Clock tick. THUS, he gets TQ-1{
        TTQ = TTQ-1;
        bob.setBurstRem( bob.getBurstRem() - (TTQ));
        if(bob.getBurstRem() > 0 && TTQ > 0){
            //Bob is Not finished, and has IO..
            PromoteIO(bob);
            Demote(bob, TTQ);
            bob.setQ(bob.getIO());
            IO_list.push_back(bob);
            Process_list.erase(Process_list.begin());
            if(!Process_list.empty()){
                CPU(Process_list.front(), 1);
            }
        }else if (bob.getBurstRem() <= 0){
            //Bob finished on or before his time was up.
            bob.setBurstRem(0);
            Output_list.push_back(bob);
            Process_list.erase(Process_list.begin());
            CPU(Process_list.front(), 1);
        }else if(bob.getBurstRem() > 0){
            //This means TTQ was 1, and this process had IO.
            
        }else{
            std::cout << "Tyler, you suck." << std::endl;
            exit(0);
        }  
    }else{
        //bob has no IO. 
        bob.setBurstRem( bob.getBurstRem() - TTQ);
        if(bob.getBurstRem() < 0 && TTQ > 0) {
            // We've finished early, so We should pull in the next process.
            Output_list.push_back(bob);
            Process_list.erase(Process_list.begin());
            if(!Process_list.empty()){
                CPU(Process_list.front(), bob.getBurstRem() * -1);
            }
        }else if(bob.getBurstRem() > 0){
            //Bob is not finished yet, No IO.
            Demote(bob,TTQ);
            Process_list.push_back(bob);
            Process_list.erase(Process_list.begin());
        }else {
            //we've finished RIGHT ON TIME~!
            Output_list.push_back(bob);
            Process_list.erase(Process_list.begin());
        }
    }
}

/* PromoteAge():
 *  Handles the periodic increases in priority. It will handle promotion toward 0.
 */       
void WHS::PromoteAge(){
    for(unsigned int i = 0; i < Process_list.size(); i++){
        // has our age timer passed?
        if((unsigned int) Process_list[i].getAge() < elapsed_Time + Process_list[i].getDynPriority()){
            //WE HAVE AGED!
            Process_list[i].setDynPriority(Process_list[i].getDynPriority() + AgeIncrement);
            Process_list[i].setAge(Process_list[i].getAge() + Agefactor);
        }
    }
}

/* PromoteIO():
 *  Handles the periodic increases in priority. It will handle promotion toward 0.
 */       
void WHS::PromoteIO(process current){
    if(current.getDynPriority() >= 50 ){
        //User band
        if(current.getDynPriority() - current.getIO() < 50){
            current.setDynPriority(50);
        }else{
            current.setDynPriority(current.getDynPriority() - current.getIO());
        }
    }else if(current.getDynPriority() >= 0){
        //Kernel band
        if(current.getDynPriority() - current.getIO() < 0){
            current.setDynPriority(0);
        }else{
            current.setDynPriority(current.getDynPriority() - current.getIO());
        }
    }
}

/* Demote():
 *  Handles the periodic decrease in priority. It will handle demotion toward 100.
 *      -Incomplete after TQ.
 */   
void WHS::Demote(process bob, int TTQ){
    if(bob.getDynPriority() >= 50 ){
        //User band
        if(bob.getDynPriority() + TTQ > bob.getPriority()){
            bob.setDynPriority(bob.getPriority());
        }else{
            bob.setDynPriority(bob.getDynPriority() + TTQ);
        }
    }else if(bob.getDynPriority() >= 0){
        //Kernel band
        if(bob.getDynPriority() + TTQ > bob.getPriority()){
            bob.setDynPriority(bob.getPriority());
        }else{
            bob.setDynPriority(bob.getDynPriority() + TTQ);
        }
    }
}

/* Step():
 *  This one must be done in single increments, unlike RTS where we can manipulate time. 
 *  Step will move a SINGLE unit of time, rather than a TQ forward. This changes how we will implement step, naturally.
 * 1. Add all new processes IMPLEMENTED
 * 2. Sort IMPLEMENTED
 * 3. Promote IMPLEMENTED
 * 4. CPU IMPLEMENTED
 * 5. Demote IMPLEMENTED
 * (6 and 7 are optional)
 */
void WHS::Step(){
    //Gather all incoming processes
    if(Input_list.size() > 0){
        while((unsigned int) Input_list.front().getArrival() == this->elapsed_Time){
            Process_list.push_back(Input_list.front());
            Input_list.erase(Input_list.begin());
        }
    }
    PromoteAge();
    std::sort(Process_list.begin(),Process_list.end(),compareWHS);
    CPU(Process_list.front(), TQ);
    if(this->elapsed_Time % 250 == 0 || this->elapsed_Time == 0){
        std::cout << "Time Passes: " << this->elapsed_Time;
        std::cout <<" Process List Size: " << Process_list.size();
        std::cout <<" Input_list Size: " << Input_list.size();
        std::cout <<" Output_list Size: " << Output_list.size() << std::endl; 
    }
this->elapsed_Time += TQ;
}

/* Print():
 *  Prints out all dat info we want to show at the end.
 * 
 */
void WHS::Print(){
    std::cout << "Printing Process_list: " << std::endl;
    for(unsigned int i = 0; i <  Process_list.size(); i++ ){
        Process_list.at(i).print();
    }
    std::cout << "Printing Output_list: " << std::endl;
    for(unsigned int c = 0; c <  Output_list.size(); c++ ){
        Output_list.at(c).print();
    }
}

bool WHS::isEmpty(){
    return Process_list.empty() && Input_list.empty();
}

bool WHS::compareWHS(process A, process B){
    if(( A.getDynPriority()) == ( B.getDynPriority() )){
        return A.getBurstRem() < B.getBurstRem();
    }else { 
        return ( A.getDynPriority() ) < ( B.getDynPriority() ); 
    }    
}

bool WHS::compareWHSArrival(process A, process B){
    if(( A.getArrival()) == ( B.getArrival() )){
        return A.getBurstRem() < B.getBurstRem();
    }else { 
        return ( A.getArrival() ) < ( B.getArrival() ); 
    }
}
