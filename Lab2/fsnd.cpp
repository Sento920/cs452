#include "fsnd.h"


/* 
 * 
 * FSND is a file transfer utility used for connecting, and sending files over a network. 
 * FSND will require two computers, a Client and a Server. 
 * The Client Sends the Server files, using a provided Address and Port.
 * Reference to stack overflow for piping: https://robots.thoughtbot.com/input-output-redirection-in-the-shell
 */

int main(int argc, char* argv[]){
    Options options;
    options.hasL = 0;
    options.hasN = 0;
    options.hasO = 0;
    options.hasH = 0;
    options.hasV = 0;
    options.hasM = 0;
    options.port = 0;
    options.offset = 0;
    options.numberOfBits = 0;
    options.fileSize = 0;
    int numberOfOptions = 0;
    int i = 0;
    while(i < argc) {
        char *j = argv[i];
    
        if(*j == '-') { //if it's an option, check the next char
            if(argv[i][1] == 'h') {
                    printf("fsnd Help Called.\n");
                    printf("-v [Verbose Mode], -l [Server Indicator], Port -p [Port Number], -o [offset], -n [Number of Bits to Send] \n");
                    printf("\n");
                    exit(0);
                    //help();
                numberOfOptions++;
            } else if(argv[i][1] == 'l') {
                    //printf("is Server\n");
                    options.hasL = 1;
                    numberOfOptions++;
            } else if(argv[i][1] == 'v') {
                    //printf("Has Verbose\n");
                    options.hasV = 1;
                    numberOfOptions++;
            } else if(argv[i][1] == 'm') {
                    //printf("Has M Flag\n");
                    options.hasM = 1;
                    numberOfOptions++;
            } else if(argv[i][1] == 'p') {
                    //printf("Has Port\n");
                    options.port = argv[i][2];
                    numberOfOptions++;
                    //Actually getting the port.
                    if(argv[i][2] != '\0') {
                        j = &argv[i][2];
                        options.port = atoi(j);
                    }else{
                        j = &argv[i+1][0];
                        options.port = atol(j);
			numberOfOptions++;
                    }
            //printf("Port: %d \n", options.port);
            } else if(argv[i][1] == 'o') {
                    options.hasO = 1;
                    numberOfOptions++;
                
                    if(argv[i][2] != '\0') {
                        j = &argv[i][2];
                        options.offset = atol(j);
                    } else {
                        j = &argv[i + 1][0];
                        options.offset = atol(j);
			numberOfOptions++;
                    }
            //printf("Offset: %ld \n", options.offset);
            } else if(argv[i][1] == 'n') {
                    options.hasN = 1;
                    numberOfOptions++;
                    if(argv[i][2] != '\0') {
                        j = &argv[i][2];
                        options.numberOfBits = atol(j);
                    } else {
                	j = &argv[i + 1][0];
                	options.numberOfBits = atol(j);
			numberOfOptions++;
            }
            //printf("NumBits: %ld \n", options.numberOfBits);
            } 
        }

        i++;

    }
    //is it the client or the server
    if(options.hasL) { //has -l (server)
        //set server program arguments
        options.serverName = "server";
        if((numberOfOptions) < argc - 1) { //if a filename exists
            options.fileName = argv[argc - 1];
        }
    } else { //not listening (client)
        //set client program arguments
        options.serverName = argv[argc - 2];
        if((numberOfOptions + 1) < argc - 1 && argv[argc-1][0] != '-') { //if a filename exists
            options.fileName = argv[argc - 1];
        }else{
         //We assume that there is no file.   
            options.fileName = "";
            options.serverName = argv[argc-1];
        }
    }
    if(!options.hasO){
        options.offset = 0;
    }

    if(options.hasL == 1){
	    if(options.hasV)
		    printf("Listening on Port: %d\n",options.port);
	    Server(options);
    }else{
	    if(options.hasV)
		    cout << "Connecting to IP: " << options.serverName << " On Port: " << options.port << " File name: " << options.fileName.c_str() << endl;
	    Client(options);  
    }
    cout << "\nThank you for Using FSND.\n";
    return 0;
}



/* void Client(Options options);
 * 0. This method will connect to an existing server, then do the following.
 * 1. it will open the file to send.
 *      We are using FILE*Fopen(const char* filename, const char* restrict mode);
 *      -Modes are -r for read, -w for write, -r+ for both
 * 2. It will write the intger number of bytes we are sending across.
 *      -WriteInt();
 * 3. It will then write the file from the offset provided.
 *      Move to the offset
 *          fseek(FILE* stream,long offset,int whence);
 *      Start to Push file
 *          fwrite(void * ptr, size_t size, size_t nItems, FILE* stream)
 *              https://www.tutorialspoint.com/c_standard_library/c_function_fwrite.htm
 *              -We want size 1, and #Items to be X.
 *      Use Ftell to ensure we know where we are IN the file
 *          Ftell(FILE* stream);
 *              https://www.tutorialspoint.com/c_standard_library/c_function_ftell.htm     
 * 
 * 4. It will then stop.
 * 
 */
void Client(Options options){
   
    char* buffer;
    int entire = 0;
    long i = 0;
    int percent;
    int curMul = 0;
    int currPer = 0;
    int noFile = 0;
    //#0
    int socket = callServer(options.serverName, options.port);
    //char* name = options.fileName.c_str();
    //#1
    if(options.fileName != ""){
        FILE* stream = fopen(options.fileName.c_str(),"r");
        if(stream == NULL) {
            printf("Error. stream from fopen is null.\n");
        }
        printf("%s\n",options.fileName.c_str());
        fseek(stream, 0, SEEK_END);
        options.fileSize = (long)ftell(stream);
        if(options.hasN == 0){
            options.numberOfBits = options.fileSize;
            entire = 1;
        }
        buffer = new char[256];
        
        //#2
        if(options.hasV){
            printf("File Size: %ld\n",options.fileSize);
            printf("Sending File [offset=%ld | sendsize = %ld]\n",options.offset,options.numberOfBits);
        }
        writeLong(options.numberOfBits,socket);
        writeLong(options.fileSize,socket);
        writeLong(entire,socket);
        //#3
        int check = fseek(stream,options.offset,SEEK_SET);
        if(check != 0){
            printf("ERROR IN FSEEK, PLEASE CONTACT YOUR SYSTEM ADMINISTRATOR\n");
            exit(0);
        }
        
        if(options.hasV){
            printf("Sending File..[");
            fflush(stdout);
        }
        while((ftell(stream) - options.offset) < options.numberOfBits && ftell(stream) != -1L){
            percent = (int)(((float)i / (float)options.numberOfBits)*100);
            if( percent % (curMul+5) == 0 && options.hasV && currPer != percent){
                printf("...%d",percent);
                fflush(stdout);
                curMul += 5;
                currPer = percent;
            }
            check = fread(buffer, sizeof(char), 255, stream);
            cout << buffer << "\n";
            if(check == 255) {
                //cout << "yuppers\n";
                writeByte(buffer, socket);
                //cout <<"Double yuppers\n";
                writeInt(0,socket);
            } else if(check <= 254 && check >= -1){
             //We've got less than 256 bits.
                //cout << "We found only " << check << " Bytes left. Adjusting length.\n";
                writeByte(buffer, socket);
                //cout << "Sending over the Amount We found.\n";
                writeInt(check,socket);
            }else{
                //Assume errors.
                printf("\nLOST BITS IN FREAD, PLEASE CONTACT YOUR SYSTEM ADMINISTRATOR\n");
                fclose(stream);
                delete[] buffer;
                exit(0);
            }
            i++;
        }
    }else{
        string s;
        noFile = 1;
        getline(std::cin, s);
        //get Filesize from Std In.
        options.fileSize = s.length();
        if(options.hasN == 0)
            options.numberOfBits = options.fileSize;
        
         buffer = new char[256];
        
        //#2
        if(options.hasV){
            printf("StdIn Size: %ld\n",options.fileSize);
            printf("Sending Input [offset=%ld | sendsize = %ld]\n",options.offset,options.numberOfBits);
        }
        writeLong(options.numberOfBits,socket);
        writeLong(options.fileSize,socket);
        writeLong(entire,socket);

        if(options.hasV){
            printf("Sending Input..[");
            cout << s;
            fflush(stdout);
        }
        
        for(unsigned int x = 0; x < s.length(); x++){
            buffer[x] = s[x];
        }
        
        while(i - options.offset < options.numberOfBits){
            percent = (int)(((float)i / (float)options.numberOfBits)*100);
            if( percent % (curMul+5) == 0 && options.hasV && currPer != percent){
                printf("...%d",percent);
                fflush(stdout);
                curMul += 5;
                currPer = percent;
            }
            if(buffer[i] != '\0') {
                writeByte(buffer, socket);
            } 
            i++;
        }
        
    }
    printf("...100]\n");
	delete[] buffer;
    printf("i = %ld",i);
    //4. We close the socket, and the file.
    if(shutdownSocket(socket, 2) != 0)
            printf("ERROR CLOSING SOCKET, PLEASE CONTACT YOUR SYSTEM ADMINISTRATOR\n");
    
    if(entire && noFile == 0){
        //Call MD5.
        printf("\nMD5 Sum:");
        fflush(stdout);
        string c = "md5sum "+ options.fileName;
        system(c.c_str());
    }
   
}


/* void Server(Options options);
 * This Method will Create a server, Then do the following
 */
void Server(Options options){
    
    int p = setupServerSocket(options.port);
    if(p){
        p = serverSocketAccept(p);
        options.numberOfBits = readLong(p);
        options.fileSize = readLong(p);
        string pL;
        int entire = readLong(p);
        //char* name = options.fileName.c_str();
        char* buffer = new char[256];
        int trail = 0;
        int percent;
        long curMul = 0;
        FILE* out;
        int currPer = 0;
        int printLater = 0;
        if(options.fileName != ""){
                out = fopen(options.fileName.c_str(), "w");
        }else{
                printLater = 1;
        }
        printf("Receiving file: %s\n",options.fileName.c_str());
        
        if(options.hasV){
            printf("Percentage Recieved: [");
            fflush(stdout);
        }
        long progress = options.numberOfBits;
        while(progress > 0){
            
            percent = (int)(((float) (options.numberOfBits - progress) / (float)options.numberOfBits)*100);
            if( percent % (curMul+5) == 0 && options.hasV && currPer != percent){
                printf("...%d",percent);
                curMul += 5;
                fflush(stdout);
                currPer = percent;
            }
            
            buffer = readByte(p);
            trail = readInt(p);
            if(options.fileName != ""){
                if(trail == 0){
                    fwrite(buffer,255,sizeof(char),out);
                    progress -= 255;
                }else{
                    fwrite(buffer,trail,sizeof(char),out);
                    progress -= trail;
                }
            }else{
                for(int k = 0; k < trail; k++){
                    pL += buffer[k];
                }
            }
            
            
        }
        if(options.hasV){
            printf("...100]\nReceive: success\n");
            printf("File Size Recieved: %ld\n",options.numberOfBits);
        }
        
        if(printLater == 1){
            cout << "\n" << pL << "\n\n";
        }
	    delete[] buffer;
        printf("%s\n",options.fileName.c_str());
        //printf("%ld", i);
        //Shutdown after file recieved.
        if(strcmp(options.fileName.c_str(),"") != 0 && fclose(out) == EOF)
            printf("ERROR CLOSING FILE, PLEASE CONTACT YOUR SYSTEM ADMINISTRATOR\n");
        
        
        if(entire && printLater == 0){
            //Call MD5.
            printf("\nMD5 Sum:");
            fflush( stdout );
            string c = "md5sum "+ options.fileName;
            system(c.c_str());
        }
        
    }else{
        printf("SETUP OF SERVER SOCKET HAS FAILED, PLEASE CONTACT YOUR SYSTEM ADMINISTRATOR\n");
    }
    printf("Server has shut down, GoodBye.\n");
}
