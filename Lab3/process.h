#ifndef PROCESS
#define PROCESS
#include <iostream>

class process{
public:
    process();
    process(int Pid, int Burst, int Arrival, int Priority, int Deadline, int IO);
    process( const process& obj);    
    process(int Pid, int Burst, int Arrival, int Priority);
    void print();
    int getPid() const;
    int getBurst() const;
    int getArrival() const;
    int getPriority() const;
    int getDeadline() const;
    int getIO() const;

    int getBurstRem() const;
    void setBurstRem(int burstRem);
    int getStart() const;
    void setStart(int start);
    int getEnd() const;
    void setEnd(int end);
    int getWaitingTime() const;
    void setWaitingTime(int waitingTime);
    int getTurnaroundTime() const;
    void setTurnaroundTime(int turnaroundTime);
    int getStartInQueue() const;
    void setStartInQueue(int startInQueue);
    int getDynPriority() const;
    void setDynPriority(int NewPiority);
    int getAge() const;
    void setAge(int NewAge);
    int getQ() const;
    void setQ(int Q);
        
    bool operator<(process other);        
private:
    int Pid;
    int Burst;
    int burstRem;
    int Arrival;
    int Priority;
    int Age;
    int DynPriority;
    int Deadline;
    int IO;
    int start;
    int startInQueue;
    int end;
    int waitingTime;
    int turnaroundTime;
    int Q;
};
#endif
