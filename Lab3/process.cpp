#include "process.h"

process::process(int Pid, int Burst, int Arrival, int Priority, int Deadline, int IO) {
	this->Pid = Pid;
	this->Burst = Burst;
	this->Arrival = Arrival;
	this->Priority = Priority;
    this->DynPriority = Priority;
    this->Age = Priority;
	this->Deadline = Deadline;
	this->IO = IO;
	this->burstRem = Burst;
	this->start = -1;
	this->startInQueue = -1;
	this->end = -1;
	this->waitingTime = -1;
	this->turnaroundTime = -1;
	this->Q = 0;
}

// condensed constructor
process::process(int Pid, int Burst, int Arrival, int Priority) {
	this->Pid = Pid;
	this->Burst = Burst;
	this->Arrival = Arrival;
	this->Priority = Priority;
	this->DynPriority = Priority;
    this->Age = Priority;
    this->Deadline = -1;
	this->IO = -1;
	this->burstRem = Burst;
	this->start = -1;
	this->startInQueue = -1;
	this->end = -1;
	this->waitingTime = -1;
	this->turnaroundTime = -1;
	this->Q = 0;
}

process::process(const process& obj) {
	this->Pid = obj.getPid();
	this->Burst = obj.getBurst();
	this->Arrival = obj.getArrival();
	this->Priority = obj.getPriority();
    this->DynPriority = obj.getDynPriority();
    this->Age = obj.getAge();
	this->Deadline = obj.getDeadline();
	this->IO = obj.getIO();
	this->burstRem = obj.getBurstRem();
	this->start = obj.getStart();
	this->startInQueue = obj.getStartInQueue();
	this->end = obj.getEnd();
	this->waitingTime = obj.getWaitingTime();
	this->turnaroundTime = obj.getTurnaroundTime();
	this->Q = obj.getQ();
}

//empty constructor
process::process() {
	this->Pid = -1;
	this->Burst = -1;
	this->Arrival = 1;
	this->Priority = -1;
    this->DynPriority = -1;
    this->Age = Priority = -1;
	this->Deadline = -1;
	this->IO = -1;
	this->burstRem = -1;
	this->start = -1;
	this->startInQueue = -1;
	this->end = -1;
	this->waitingTime = -1;
	this->turnaroundTime = -1;
	this->Q = 0;
}

int process::getPid() const {
	return this->Pid;
}

int process::getBurst() const  {
	return this->Burst;
}

int process::getArrival() const {
	return this->Arrival;
}

int process::getPriority() const {
	return this->Priority;
}

int process::getDynPriority() const{
    return this->DynPriority;
}

void process::setDynPriority(int NewPriority){
    this->DynPriority = NewPriority;
}

int process::getAge() const{
    return this->Age;
}

void process::setAge(int NewAge){
    this->Age = NewAge;
}

int process::getDeadline() const {
	return this->Deadline;
}

int process::getIO() const {
	return this->IO;
}

int process::getBurstRem() const {
	return this->burstRem;
}

void process::setBurstRem(int burstRem) {
	this->burstRem = burstRem;
}

int process::getStart() const {
	return this->start;
}

void process::setStart(int start) {
	this->start = start;
}

int process::getEnd() const {
	return this->end;
}

void process::setEnd(int end) {
	this->end = end;
}

int process::getWaitingTime() const {
	return this->waitingTime;
}

void process::setWaitingTime(int waitingTime) {
	this->waitingTime = waitingTime;
}

int process::getTurnaroundTime() const {
	return this->turnaroundTime;
}

void process::setTurnaroundTime(int turnaroundTime) {
	this->turnaroundTime = turnaroundTime;
}

int process::getStartInQueue() const {
	return this->startInQueue;
}

void process::setStartInQueue(int startInQueue) {
	this->startInQueue = startInQueue;
}

bool process::operator<(process other) {
	return this->Arrival > other.getArrival();
}

int process::getQ() const {
	return this->Q;
}

void process::setQ(int Q) {
	this->Q = Q;
}

void process::print(){
    std::cout << "PID: " << this->Pid;
    std::cout << " Burst: "<< this->Burst;
    std::cout << " RemBurst: "<< this->burstRem;
    std::cout << " Arrival: "<< this->Arrival;
    std::cout << " Priority: "<< this->Priority;
    std::cout << " Deadline: "<< this->Deadline;
    std::cout << " IO: " << this->IO << std::endl;
    std::cout << "Started at: " << this->start;
    std::cout << "  Ended at: " << this->end;
    std::cout << "  Waited: " << ((end - Burst) + burstRem) << " cycles..\n" << std::endl;
}
