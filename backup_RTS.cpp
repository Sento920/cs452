#include "schedule.h"
#define DEBUG_2
#define DEBUG


//Constructor
RTS::RTS(int TQ, std::vector<process> Input, bool isHard){
    this->Input_list = Input;
    std::sort(Input_list.begin(),Input_list.end(),compareRTSArrival);
    Process_list.clear();
    Output_list.clear();
    this->elapsed_Time = 0;
    this->TQ = TQ;   
    this->Hard = isHard;
#ifdef DEBUG
        std::cout << "At Initialization... ";
        std::cout <<" Process List Size: " << Process_list.size();
        std::cout <<" Input_list Size: " << Input_list.size();
        std::cout <<" Output_List Size: " << Output_list.size() << std::endl; 
#endif
}

//Does the CPU editing to Bob.
void RTS::CPU (process bob, int TTQ){
#ifdef DEBUG_2
    std::cout << "TTQ: " << TTQ << std::endl;
    std::cout << "Bob REM: " << bob.getBurstRem() << std::endl;
    std::cout << "Bob REM - TTQ: " << (bob.getBurstRem() - TTQ) << std::endl;
    std::cout << "CTQ Process Size: " << Process_list.size() << std::endl;
    std::cout << "CTQ Input Size: " << Input_list.size() << std::endl;
    std::cout << "CTQ Out Size: " << Output_list.size() << std::endl;
    
#endif
    
    bob.setBurstRem( bob.getBurstRem() - TTQ);
    if(bob.getBurstRem() > 0){
        //Bob is Not finished.
        Process_list.push_back(bob);
        Process_list.erase(Process_list.begin());
        
    }else if(bob.getBurstRem() < 0 && TTQ > 0) {
        // We've finished early, so We should pull in the next process.
        Output_list.push_back(bob);
        Process_list.erase(Process_list.begin());
        CPU(Process_list.front(), bob.getBurstRem() * -1);
    }else if(bob.getBurstRem() == 0){
        //we've finished RIGHT ON TIME~!
        Output_list.push_back(bob);
        Process_list.erase(Process_list.begin());
    }
#ifdef DEBUG_2
    bob.print();
#endif
}

/* 
 * RTS::Step();
 * 
 * Description: This method moves time forward one step.
 * 
 * This involves (in no particular order):
 * 1. Checking the process that was just on the CPU vs time Quantum.
 * 2. Checking the current Input to see if any NEED the CPU.
 * 3. Killing any processes that did not make the deadline (IMPLEMENTED)
 * 4. Getting a process on the CPU.
 * 5. increment Time Elapsed. (IMPLEMENTED)
 * 6. Sort the vector, using comparator(IMPLEMENTED).
 */
void RTS::Step(){
    //gather new processes.
    if(Input_list.size() > 0){
        while((unsigned int) Input_list.front().getArrival() == this->elapsed_Time){
        Process_list.push_back(Input_list.front());
        Input_list.erase(Input_list.begin());
    
        }
    }
    
    if(Process_list.size() > 0){
        std::sort(Process_list.begin(),Process_list.end(),compareRTS);
        CPU(Process_list.front(),this->TQ);
        
        //Run through the Process List, change anything that needs to be changed.
        for(unsigned int i = 0; i < Process_list.size(); i++){
            if((unsigned int) Process_list.at(i).getDeadline() < this->elapsed_Time || (unsigned int) Process_list.at(i).getBurstRem() > Process_list.at(i).getDeadline() - this->elapsed_Time){
                Dead_list.push_back(Process_list.at(i));
                Process_list.erase(Process_list.begin()+i);
            }
        }
    }
    this->elapsed_Time += TQ;
    if(this->elapsed_Time % 500 == 0){
        std::cout << "Time Passes: " << this->elapsed_Time;
        std::cout <<" Process List Size: " << Process_list.size();
        std::cout <<" Input_list Size: " << Input_list.size();
        std::cout <<" Output_List Size: " << Output_list.size(); 
        std::cout <<" Dead: " << Dead_list.size() << std::endl; 
    }
}

void RTS::Print(){
    std::cout << "\n\n\nPrinting Process_list: " << std::endl;
    for(unsigned int i = 0; i <  Process_list.size(); i++ ){
        Process_list.at(i).print();
    }
    std::cout << "\n\n\nPrinting Output_list: " << std::endl;
    for(unsigned int c = 0; c <  Output_list.size(); c++ ){
        Output_list.at(c).print();
    }
    /*
    std::cout << "Printing Dead process List: " << std::endl;
    for(unsigned int i = 0; i <  Dead_list.size(); i++ ){
        Dead_list.at(i).print();
    }
    */
}

bool RTS::isHard(){
    return this->Hard;
}

bool RTS::isEmpty(){
    return Input_list.empty() && Process_list.empty();
}

//checks to see if B is greater than A.
bool RTS::compareRTS(process A, process B){
    if(( A.getDeadline()) == ( B.getDeadline() )){
        return A.getBurstRem() < B.getBurstRem();
    }else { 
        return ( A.getDeadline() ) < ( B.getDeadline() ); 
    }
}

bool RTS::compareRTSArrival(process A, process B){
    if(( A.getArrival()) == ( B.getArrival() )){
        return A.getBurstRem() < B.getBurstRem();
    }else { 
        return ( A.getArrival() ) < ( B.getArrival() ); 
    }
}

