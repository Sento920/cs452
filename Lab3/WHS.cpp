#include "schedule.h"



/*
 * Class: Windows Hybrid Scheduler
 * Description:
 * A dynamic scheduler that works off a determined priority number.
 * Priority is determined from read in (This is the base val, cannot go below that)
 *  - Increased through age, and length of IO. 
 *      - Age is a number of clock ticks since it last got the cpu.
 *      - IO is done at the second to last piece of its burst, unless process ends before that.
 *  - Decreased if it lasts more than 1 time quantom buy the length of that quantom.
 *  - There are 2 "Bands" of priority: those below 49, and those above it. 0 (highest) -> 100 (lowest) inclusive.
 * The bands will be maintained inside of Promote and Demote, since we will already have our hands in the guts. This should be pretty easy to maintain, since we have the "Concrete" Priority around to see which band it is in.
 */ 



WHS::WHS(std::vector<process> Input, int TTQ){
    this->Input_list = Input;
    std::sort(Input_list.begin(),Input_list.end(),compareWHSArrival);
    Input.front().print();
    this->Process_list.clear();
    this->Output_list.clear();
    this->TQ = TTQ;
    this->elapsed_Time = 0;
    this->Agefactor = 100;
    this->AgeIncrement = 10;
    this->Input_Empty = false;
    this->Process_Empty = false;
    current = process();
    new_pro = process(1000000,100000,100000,100000,100000,100000);
        
    #ifdef DEBUG
    std::cout << "At Initialization... ";
    std::cout <<" Process List Size: " << Process_list.size();
    std::cout <<" Input_list Size: " << Input_list.size();
    std::cout <<" Output_List Size: " << Output_list.size(); 
    std::cout <<" TQ: " << this->TQ << std::endl;
    Print();
	#endif
}

/*CPU():
 * Does the editing for a single active process. 
 * Should be called Semi-recursively using Time Quantum reduction Concepts
 * Should be done After promotion, and before demotion.
 * Will probably be really complicated, as there is a lot of things to keep track of.
 */
process WHS::CPU(process bob){
#ifdef DEBUG
    std::cout << "Time: " << this->elapsed_Time << std::endl;
    std::cout << "Bob REM: " << bob.getBurstRem() << std::endl;
    std::cout << "Bob Dynamic Priority: " << bob.getDynPriority() << std::endl;
    std::cout << "Bob Static Priority: " << bob.getPriority() << std::endl;
    std::cout << "CTQ Process Size: " << Process_list.size() << std::endl;
    std::cout << "CTQ Input Size: " << Input_list.size() << std::endl;
    std::cout << "CTQ Out Size: " << Output_list.size() << std::endl;
#endif
    if(bob.getStart() == -1)
        bob.setStart(elapsed_Time);
    bob.setBurstRem( bob.getBurstRem() - 1);
    bob.setQ(bob.getQ()-1);
    
#ifdef DEBUG
    std::cout << "Just after CPU edit" << std::endl;
    std::cout << "bob.Q: " << bob.getQ() << std::endl;
    bob.print();
#endif
    return bob;

}

/* PromoteAge():
 *  Handles the periodic increases in priority. It will handle promotion toward 0.
 */       
void WHS::PromoteAge(){
    for(unsigned int i = 0; i < Process_list.size(); i++){
        // has our age timer passed?
        if((unsigned int) Process_list[i].getAge() < elapsed_Time + Process_list[i].getDynPriority()){
            //WE HAVE AGED!
            Process_list[i].setDynPriority(Process_list[i].getDynPriority() + AgeIncrement);
            Process_list[i].setAge(Process_list[i].getAge() + Agefactor);
        }
    }
}

/* PromoteIO():
 *  Handles the periodic increases in priority. It will handle promotion toward 0.
 */       
process WHS::PromoteIO(process current){
    if(current.getDynPriority() >= 50 ){
        //User band
        if(current.getDynPriority() - current.getIO() < 50){
            current.setDynPriority(50);
        }else{
            current.setDynPriority(current.getDynPriority() - current.getIO());
        }
    }else if(current.getDynPriority() >= 0){
        //Kernel band
        if(current.getDynPriority() - current.getIO() < 0){
            current.setDynPriority(0);
        }else{
            current.setDynPriority(current.getDynPriority() - current.getIO());
        }
    }
    return current;
}

/* Demote():
 *  Handles the periodic decrease in priority. It will handle demotion toward 100.
 *      -Incomplete after TQ.
 */   
process WHS::Demote(process bob, int TTQ){
    if(bob.getDynPriority() >= 50 ){
        //User band
        if(bob.getDynPriority() + TTQ > bob.getPriority()){
            bob.setDynPriority(bob.getPriority());
        }else{
            bob.setDynPriority(bob.getDynPriority() + TTQ);
        }
    }else if(bob.getDynPriority() >= 0){
        //Kernel band
        if(bob.getDynPriority() + TTQ > bob.getPriority()){
            bob.setDynPriority(bob.getPriority());
        }else{
            bob.setDynPriority(bob.getDynPriority() + TTQ);
        }
    }
    return bob;
}

/* Step():
 *  This one must be done in single increments, unlike RTS where we can manipulate time. 
 *  Step will move a SINGLE unit of time, rather than a TQ forward. This changes how we will implement step, naturally.
 * 1. Add all new processes IMPLEMENTED
 * 2. Sort IMPLEMENTED
 * 3. Promote IMPLEMENTED
 * 4. CPU IMPLEMENTED
 * 5. Demote IMPLEMENTED
 * (6 and 7 are optional)
 */
void WHS::Step(){
    new_pro = process(1000000,100000,100000,100000,100000,100000);
    //gather new processes.
    if(!Input_Empty){
        while((unsigned int) Input_list.front().getArrival() == this->elapsed_Time){
            //If front is better than current new best...      
            if(compareWHS(Input_list.front(),new_pro)){
                new_pro = Input_list.front();
            }
            Process_list.push_back(Input_list.front());
            Input_list.erase(Input_list.begin());
            if(Input_list.size() == 1){
                // We have the last input, So we need to make sure we stop Input here.
                new_pro = Input_list.front();
#ifdef DEBUG
                std::cout << "Last Input, Nearing Completion. Remaining Processes: " << std::endl;
                new_pro.print();
#endif
                Input_list.erase(Input_list.begin());
                Input_Empty = true;
            }
        }
    }else{
        //Provide a "New" Process.
         new_pro = process(1000000,100000,100000,100000,100000,100000);
    }

//Do all the Process stuff    
    if(Process_list.size() > 0){    
        //Go get all the current IO processes,
        for(unsigned int i = 0; i < IO_list.size(); i++){
            if(IO_list[i].getQ() == 0){
#ifdef DEBUG
                std::cout << "IO.Qb: " << IO_list.front().getQ() << std::endl;
                IO_list[i].print();
#endif
                IO_list[i].setQ(TQ-1);
#ifdef DEBUG
                std::cout << "bob.Qa: " << IO_list.front().getQ() << std::endl;
                IO_list[i].print();
#endif
                Process_list.push_back(IO_list[i]);
                IO_list.erase(IO_list.begin()+i);
                i--;
                std::sort(Process_list.begin(),Process_list.end(),compareWHS);
            }else{
                IO_list[i].setQ(IO_list[i].getQ()-1);
            }
        }
        // check Pre-emption, Time Interrupt, Finishing, and current's Deadline.
        PromoteAge();
#ifdef DEBUG
        std::cout << "\nCurrent Process Lists" << std::endl;
        Print();
        std::cout << "\nnewpro" << std::endl;
        if(new_pro.getPid() != 1000000)
            new_pro.print();
        std::cout << "curr" << std::endl;
        current.print();
#endif
        if(compareWHS(new_pro,current) || elapsed_Time % TQ == 0 || current.getBurstRem() < 1){
            // We need the newest process to go.
            if(elapsed_Time % TQ == 0){
                //current is being Kicked out.
                Demote(current,TQ);
                Process_list.push_back(current);
            }else if(current.getBurstRem() < 1){
                //We've finished~!
                current.setEnd(elapsed_Time);
                Output_list.push_back(current);
            }else{
                //We must have a better process.
                Process_list.push_back(current);
            }
            //We've done all checks for current, On to work~!

            std::sort(Process_list.begin(),Process_list.end(),compareWHS);
            current = CPU(Process_list.front());
            Process_list.erase(Process_list.begin());
            
        }else{
            //run our best process~!
            current = CPU(current);
            if(current.getQ() == 0 && current.getIO() > 0){
             //Q0 means IO!
#ifdef DEBUG
                std::cout << "IO" << std::endl;
                current.print();
#endif
                current = PromoteIO(current);
                current.setQ(current.getIO());
                IO_list.push_back(current);
                current = process();
            }
        }
    }

    
    this->elapsed_Time++;
#ifdef DEBUG
    std::cout << "\n Time Passes: " << this->elapsed_Time << std::endl;
    if(this->elapsed_Time % TQ == 0){
        std::cout <<"Time Interrupt~!" << std::endl;
        std::cout <<" Process List Size: " << Process_list.size();
        std::cout <<" Input_list Size: " << Input_list.size();
        std::cout <<" Output_List Size: " << Output_list.size(); 
        std::cout <<" IO_List Size:" << IO_list.size(); 
    }
#endif 
}

/* Print():
 *  Prints out all dat info we want to show at the end.
 * 
 */
void WHS::Print(){
    std::cout << "\nPrinting Process_list: " << std::endl;
    for(unsigned int i = 0; i <  Process_list.size(); i++ ){
        Process_list.at(i).print();
    }
    std::cout << "\nPrinting Output_list: " << std::endl;
    for(unsigned int c = 0; c <  Output_list.size(); c++ ){
        Output_list.at(c).print();
    }
     std::cout << "\nPrinting Input_list: " << std::endl;
    for(unsigned int c = 0; c <  Input_list.size(); c++ ){
        Input_list.at(c).print();
    }
    std::cout << "\nPrinting IO_list: " << std::endl;
    for(unsigned int c = 0; c <  IO_list.size(); c++ ){
        IO_list.at(c).print();
    }
}

bool WHS::isEmpty(){
    return Process_list.empty() && Input_list.empty() && IO_list.empty();
}

bool WHS::compareWHS(process A, process B){
    if(( A.getDynPriority()) == ( B.getDynPriority() )){
        return A.getBurstRem() < B.getBurstRem();
    }else { 
        return ( A.getDynPriority() ) < ( B.getDynPriority() ); 
    }    
}

bool WHS::compareWHSArrival(process A, process B){
    if(( A.getArrival()) == ( B.getArrival() )){
        return A.getBurstRem() < B.getBurstRem();
    }else { 
        return ( A.getArrival() ) < ( B.getArrival() ); 
    }
}
