#include "schedule.h"
#define DEBUG

std::vector<process> Processify(std::string file, std::string scheduler);
process ReadFile(std::string filename, std::string scheduler);

int main (){
    std::string user;
    std::string file;
    std::string TQ;
    std::string Age;
    std::string num;
    std::cout << "Hello User. Would you like to use an MFQS, RTS, or WHS?:" << std::endl;
    std::cin >> user;
    if(user.at(0) == 'M'||user.at(0) == 'R' ||user.at(0) == 'W'){
        //We know the Queue type now.
        std::cout << "Please put in a file name for input to the selected Scheduler" << std::endl;
        std::cin >> file;
        if(user.at(0) == 'M'){
            std::cout << "Please input how long of Time Quantums you would like to use. Units: (1 step : 1 age)" << std::endl;
            std::cin >> TQ;
            std::cout << "Please input how many Queues you would like to use: " << std::endl;
            std::cin >> num;
            std::cout << "Please input how long for Aging you would like to use: " << std::endl;
            std::cin >> Age;
            std::vector<process> processes = Processify(file, "M");
            MFQS Current( std::stoi(num.c_str()),std::stoi(TQ.c_str()),processes,std::stoi(Age.c_str()));
            //DO THE THINGS.
            Current.Step();
        }else if (user.at(0) == 'R'){
            //Real Time Scheduler
            std::cout << "Please input how long of Time Quantums you would like to use. Units: (1 step : 1 age)" << std::endl;
            std::cin >> TQ;
            RTS Current(std::stoi(TQ.c_str()),Processify(file, "R"),false);
            std::cout << "We are starting Real Time Scheduling." << std::endl;
            while(!Current.isEmpty()){
                Current.Step();
            }
            std::cout << "Finished RTS." << std::endl;
            Current.Print();
        }else if (user.at(0) == 'W'){
            //Windows Hybrid Scheduler
            std::cout << "Please input how long of Time Quantums you would like to use. Units: (1 step : 1 age)" << std::endl;
            std::cin >> TQ;
            std::cout << "Starting Input Processing..." << std::endl;
           // std::cout << "TQ: " << TQ << " C: " <<TQ.c_str() << " CNUM: " << std::stoi(TQ.c_str()) << std::endl; 
	    WHS Current(Processify(file, "W"),std::stoi(TQ.c_str()));
            std::cout << "We are starting Windows Hybrid Scheduling.." << std::endl;
            while(!Current.isEmpty()){
                Current.Step();
            }
            std::cout << "Finished WHS." << std::endl;
            Current.Print();
        }   
    }else{
        std::cout << "Incorrect Input Detected, Please restart!" << std::endl;
        return 0;
    }
    std::cout << "Thank you for using the TMScheduler." << std::endl;
 return 0;   
}


std::vector<process> Processify(std::string file, std::string scheduler){
    std::vector<process> out;
    std::ifstream inputStream;
    inputStream.open(file);
    std::string x;
    //first line is GARBAGE.
    std::getline(inputStream,x);
    //long i = 0;
    while(!inputStream.eof()){
        //std::cout << i << std::endl;
        std::getline(inputStream,x);
        if(x != "" ){
            process c = ReadFile(x, scheduler);
        //std::cout << x;
        if(c.getPid() != -1)
            out.emplace_back(c);
        //i++;
        }
    }
    return out;
}


 process ReadFile(std::string line, std::string scheduler){
    int Pid, Burst, Arrival, Priority, Deadline, IO;
    std::stringstream buffer(line);
    process temp = process();
    buffer >> Pid;
    buffer >> Burst;
    buffer >> Arrival;
    buffer >> Priority;
    buffer >> Deadline;
    buffer >> IO;

    if(scheduler.compare("M") == 0) {
    	if(Pid >= 0 && Burst > 0 && Arrival >= 0 && Priority >= 0 ){
        	temp = process(Pid,Burst,Arrival,Priority,Deadline,IO);
        	#ifdef DEBUG_2
            	temp.print();
        	#endif
	}

    } else if(scheduler.compare("R") == 0) {
    	if(Pid >= 0 && Burst > 0 && Arrival >= 0 && Deadline >= 0 ){
    	    temp = process(Pid,Burst,Arrival,Priority,Deadline,IO);
    	    #ifdef DEBUG_2
            temp.print();
            #endif
    	}else{
            if(false){
                std::cout << "HARD STOP ENCOUNTERED, ABORTING." << std::endl;
                exit(0);
            }else{
		#ifdef DEBUG_2
            	    std::cout << "SOFT STOP ENCOUNTERED, CONTINUING AFTER THROW." <<std::endl;
            	    temp.print();
		#endif
                temp = process(-1,Burst,Arrival,Priority,Deadline,IO);
            }
        }

    } else { //its W
    	if(Pid >= 0 && Burst > 0 && Arrival >= 0 && Priority >= 0 && IO >= 0 ){
        	temp = process(Pid,Burst,Arrival,Priority,Deadline,IO);
        	#ifdef DEBUG_2
            	    temp.print();
        	#endif
    	}

    }
    return temp;
}
