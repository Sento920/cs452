#include <string>
#include <iostream>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

using std::string;
using std::strcmp;
using std::cout;
using std::endl;

typedef struct Flags {
    int hasH; //boolean --help screen flag
    int hasL; //boolean --listening flag
    int hasV; //boolean --verbose flag (extra info)
    int hasM; //boolean --Server takes in offset and numberOfBits --extra credit
    int hasN; //boolean --Has specific Number of bits to send.
    int hasO; //boolean --Has a specific offset to use from the beginning of the file.
    int port;
    long offset;
    long numberOfBits;
    long fileSize;
    string serverName;
    string fileName;
} Options;

//used in fsnd.c
 void Client(Options options);
 void Server(Options options);

 
 //Declared in UWECSOCKET
 int setupServerSocket(int portno); // Like new ServerSocket in Java
 int callServer(string host, int portno); // Like new Socket in Java
 int serverSocketAccept(int serverSocket); // Like ss.accept() in Java
 void writeLong(long x, int socket); // Write an int over the given socket
 long readLong(int socket); // Read an int from the given socket
 char* readByte(int socket); // Read a Byte from the given socket
 void writeByte(char* x, int socket); //write a Byte from the given socket
 int shutdownSocket(int socket, int flags);//Shut Down Socket after use.
 void writeInt(int x, int socket);
 int readInt(int socket);
