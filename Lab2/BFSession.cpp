#include <iostream>
#include <cstring>
#include <cstdlib>
#include <stdlib.h>
#include <cstdint>
#include <string>
#include <cstring>
#include <sstream>
#include "BFSession.h"
#include <vector>

using std::strcmp;
using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::toupper;
using std::hex;
using std::istringstream;
using std::getline;
using std::to_string;
using std::vector;
//using std::cstring;

//KDC handles inputs.

//IDA will close all socket connections.

//IDB will handle whatever.


uint64_t* stringtoHex(string stringX, int* out){
    //Set up at 64 bit array of Hex values, of String/8 long.
    int length = (stringX.length()/8 + stringX.length()%8);
	uint64_t* hexVal = new uint64_t [length];
    for(int i = 0; i < length; i++){
        hexVal[i] = 0;
        for(int c = 0; c < 8; c++){
            hexVal[i] <<= 8;
            hexVal[i] += stringX[(i*8) + c];
        }
    }
    *out = length;
    //cout << "hexVal: "<< hex << hexVal[0] << "\n";
    return hexVal;
}

string hextoString(uint64_t hexVal[], int length){
    string x;
    for(int i = 0; i < length; i++){
        string y;
        for(int c = 0; c < 8; c++){
            //cout << hexVal[i] << " : String So far: ";
            y += hexVal[i] & 0xFF;
            //cout << x << " at (i, c) = (" << i << ", " << c << ")\n";
            hexVal[i] >>= 8;
        }
        for(int v = y.length(); v >=0; v--){
            x+= y[v];
        }
    }
    return x;
}


string stringToUpper(string s) {
	for (unsigned int i = 0; i < s.size(); i++) {
		s[i] = toupper(s[i]);
	}
	return s;
}

int KDC() {
	string Ks;
	string Ka;
	string Kb;
    string Host_addr;
    string request;
	string ida;
    string filename;
    int port = 0;
	//unsigned long int nonce;

	Blowfish BF;
    cout << "Please enter the Port to Host the KDC: ";
	cin >> port;
	cin.ignore();
	cout << "Please enter a session key to use for distribution: ";
	cin >> Ks;
	cin.ignore();
	cout << "Please enter a master key (to authenticate Ka): ";
	cin >> Ka;
	cin.ignore();
	cout << "Please enter a master key (to authenticate Kb, and Pass to Ka.): ";
	cin >> Kb;
	cin.ignore();

	cout << "port: " << port << endl;
	cout << "Ks: " << Ks << endl;
	cout << "Ka: " << Ka << endl;
	cout << "Kb: " << Kb << endl;    
    cout << "File: " << filename << endl;
    
    cout << "Now waiting for a connection." << endl;
	int socket = setupServerSocket(port);
	if(socket) {
        // Set up socket, read Request string, and Nonce.
		int IDA_socket = serverSocketAccept(socket);
		//request = readString(IDA_socket);

        request = " Request Ks for Kb";
	ida = "thing1";	

	cout << "Request Received: " << request << endl;

        char* nonceA = readByte(IDA_socket);

	printf("Nonce: %s\n", nonceA);

        string x;
        for(unsigned int v = 0; v < sizeof(nonceA); v++){
             x += nonceA[v];
        }
        cout << "Nonce Recieved from IDA: " << x << endl;        
        // Encrypt Ksession, Kida using Kb Then add them to the larger string.
        string Ekb = Ks + "," + ida;
        cout << Ekb << endl;
	
	int Ekb_length = 0;
	uint64_t* Ekbhex = stringtoHex(Ekb, &Ekb_length);

	cout << "Ekb in hex: " << hex << Ekbhex << endl;	

        BF.Set_Passwd(Kb.c_str());
       	BF.Encrypt(&Ekbhex, sizeof(Ekbhex));


	cout << "Ekb: " << hex << Ekbhex << endl;
	cout << "Len: " << Ekb_length << endl;

	cout << Ks << endl;
	cout << x << endl;
	cout << request << endl;
	cout << ida << endl;
		
	//string temp = "ABCDEFGHIJKLMNOPqrstuvWX";
	string temp = Ks + " | " + request + " | " + x;
        cout << temp << endl;
	cout << temp.length() << endl;

        //Convert larger string to hex, then Send across.
        int Eka_length = 0;
        uint64_t* Eka = stringtoHex(temp, &Eka_length);

	cout << "Eka length: " << Eka_length << endl;
        
	//combine packets
	int Ekab_length = Eka_length + Ekb_length;
	uint64_t* packet = new uint64_t[Ekab_length];
	for(int i = 0; i < Eka_length; i++) {
		packet[i] = Eka[i];
	}        
	for(int i = Eka_length - 1; i < Ekab_length; i++) {
		packet[i] = Ekb[i];
	}

	//Eka is now a Hex value for the long string.. Set up BF to encryt using Ka, then encrypt the Hex value.
        BF.Set_Passwd(Ka.c_str());
        for(int i = 0; i < Ekab_length; i++){
            BF.Encrypt(&packet[i], sizeof(packet[i]));
        }

    cout << "BF Encrypted Session for Ka." << endl;
    cout << "Writing back to IDA.\n";
    cout << "EKA Len: " << Eka_length << " EKB Len: " << Ekb_length << endl;
	cout << "EKAB Length: " << Ekab_length << endl;
	writeInt(Ekab_length, IDA_socket);
        for(int i = 0; i < Ekab_length; i++){
            writeLongInt(packet[i],IDA_socket);
        }
        cout << "Wrote out to the IDA. Our Job is done here.\n";
	} else {
		cout << "Setup of the server socket has failed." << endl;
	}
	
	cout << "The KDC is done distributing. Shutting down." << endl;	
	return 0;
}

int IDA() {
    string KaNonce;
	string Ka;
	string Host_addr;
    string filename;
    int Host_port = 0;
	Blowfish BF;
    
    cout << "Please enter the filename. ";
    cin >> filename;
    cin.ignore();
    cout << "Please enter the KDC's Host Name: ";
    cin >> Host_addr;
    cin.ignore();
    cout << "Please enter the Port to Access the KDC: ";
	cin >> Host_port;
	cin.ignore();
	cout << "Please enter your nonce (N1): ";
	cin >> KaNonce;
	cin.ignore();
	cout << "Please enter your Master Key (Ka): ";
	cin >> Ka;
	cin.ignore();
    cout << "Please enter the name of the file you'd like to send.";
    cin >> filename;
    cin.ignore();

	cout << "port: " << Host_port << endl;
	cout << "KaNonce: " << KaNonce << endl;
	cout << "Ka: " << Ka << endl;
	int socket = callServer(Host_addr, Host_port);
    cout << "KDC is accessed.\n";
	
     string request = "Requesting Ks for IDB";
	cout << request << endl;
    	cout << "Sending nonce: " << KaNonce << endl;
	
    char x[sizeof(KaNonce)];
	memset(x, 0, sizeof(x));
	memcpy(x, KaNonce.c_str(), KaNonce.size() + 1);

    writeByte(x, socket);
    
    cout << "Wrote out the Nonce, waiting to read new length and Encrypted Nonce\n";
    int length = readInt(socket);
    cout << "Read in Length." << length << endl;
    // All our problems stem from here, to the next comment. 
    string Eka;
    uint64_t* encNonce = new uint64_t[length];
	for(int i = 0; i < length; i++) {
		memset(&encNonce[i], 0, sizeof(uint64_t));
	}

    BF.Set_Passwd(Ka.c_str());
    for(int i = 0; i < length; i++){
        encNonce[i] = readLongInt(socket);
        //cout << encNonce[i] << " <- ENonce" << endl;
        BF.Decrypt(&encNonce[i],sizeof(encNonce[i]));
        Eka += hextoString(&encNonce[i], length);
    }
    cout << "Encrypted String gathered from encrypted Session Key|Nonce|Request|Encrypted Kb.\n" << "String: " << Eka << "\n";
    shutdownSocket(socket,2);
    
    
    //This is where we need to tear apart the Nonce Encryption to get B's Info.
    int last_divider = Eka.find_last_of("|") + 1;
    int first_divider = Eka.find_first_of("|") - 1;
    
    string Eks = Eka.substr(0 , first_divider);
    string Ekb = Eka.substr(last_divider ,string::npos);
    
    cout << "connecting to B";
    Host_port = 9000;
    Host_addr = "localhost";
    socket = callServer(Host_addr, Host_port);
    cout << "Calling B." << endl;
    // Write length and Eb over.
    writeInt(Ekb.length(),Host_port);
    
    
    for(unsigned int i = 0; i < Ekb.length(); i++){
        writeLongInt(Ekb[i],socket);
    }    
    
    shutdownSocket(socket,2);
    
    
    //Encrypt the file!
    // Take the old bytes, and put them into uint64_t.
    FILE* stream = fopen(filename.c_str(),"r");
    FILE* out = fopen("t.enc","w");
        if(stream == NULL) {
            printf("Error. stream from fopen is null.\n");
        }else{
         //file is open.
            while(ftell(stream) != -1L){
                fseek(stream, 0, SEEK_END);
                long filesize = (long)ftell(stream);
                // %8 because if its not encrypted, it should only be mod 8 by chance.
                if(filesize%8 == 0  && (filesize -128) > 0){
                    char* buffer = new char[129];
                    string buff;
                    int check = fread(buffer, sizeof(char), 128, stream);
                    if(check == 128){
                        int Elength = 0;
                        uint64_t* packet;
                        packet = stringtoHex(buffer, &Elength);
                        memset(buffer, 0, sizeof(uint64_t));
                        for(int i = 0; i < Elength; i++) {
                            BF.Encrypt(&packet[i], sizeof(packet[i]));
                        }
                        buff = hextoString(packet,Elength);
                        fwrite(buff.c_str(),buff.length(),sizeof(buff),out);
                        filesize = filesize - 128;
                    }else{
                    //this only happens at end of file.
                        filesize = 128 - filesize;
                        int Elength = 0;
                        uint64_t* packet;
                        packet = stringtoHex(buffer, &Elength);
                        memset(buffer, 0, sizeof(uint64_t));
                        for(int i = 0; i < Elength; i++) {
                            BF.Encrypt(&packet[i], sizeof(packet[i]));
                        }
                        buff = hextoString(packet,Elength);
                        fwrite(buff.c_str(),buff.length(),sizeof(buff),out);
                        filesize = 0;
                        delete[] buffer;
                        cout << "Thank you for using BFSession." << endl;
                        //Call fsnd as a Server!
                        string fsnd( "./fsnd -v -p " + std::to_string(Host_port) + " " + Host_addr.c_str()  + " t.enc");
                        system(fsnd.c_str());
                    }
                    
                }
            }
        }
	return 0;
}
    


int IDB() {
	string KbNonce;
    unsigned long int nonce;
	string Kb;
    int port;
	Blowfish BF;
    

    cout << "Please enter the Port to Setup Server: ";
	cin >> port;
	cin.ignore();
	cout << "Please enter your nonce (N2): ";
	cin >> KbNonce;
	cin.ignore();
	nonce = atol(KbNonce.c_str());
	cout << "\nPlease enter your Master Key (Kb): ";
	cin >> Kb;
	cin.ignore();
   

	cout << "nonce2: " << nonce << endl; 
	//done with key exchange
	
	//testing bf encryption
	int Host_socket = setupServerSocket(port);
    int Ks_len = 0;
    uint64_t Ks;
    Ks_len = readInt(Host_socket); 
    Ks = readLongInt(Host_socket);
	BF.Set_Passwd(Kb.c_str());
    BF.Decrypt(&Ks,sizeof(Ks));
    string Ekb = hextoString(&Ks,Ks_len);
    cout << Ekb << endl;
    // We now have the "Nonce" or Kx and IDA.
    int loc = -1;
    for(unsigned int c = 0; c < Ekb.length(); c++){
        if(Ekb.at(c) == ','){
            loc = c;
        }
    }
    string Kss = Ekb.substr(0, loc);
    BF.Set_Passwd(Kss.c_str());
    uint64_t* Kb_Nonce_long = stringtoHex(Kss, &Ks_len);
    for(int i = 0; i < Ks_len; i++){
        BF.Encrypt(&Kb_Nonce_long[i], sizeof(uint64_t));
    }
    //We now have the Key! Time to send it!
    for(int i = 0; i < Ks_len; i++){
        writeLongInt(Kb_Nonce_long[i],Host_socket);
    }
    //Wait for the return function.
    Ks_len = readInt(Host_socket);
    uint64_t* Eks_Nb = new uint64_t[Ks_len];
    for(int i = 0; i < Ks_len; i++){
        Eks_Nb[i] = readLongInt(Host_socket);
    }
    shutdownSocket(Host_socket,2);
    //We dont need IDA anymore, Just Fsnd!!
    
    //Call fsnd as a Server!
    string fsnd( "./fsnd -l -v -p " + std::to_string(port) + " i.enc");
    system(fsnd.c_str());
    //Decrypt the file!
    // Take the old bytes, and put them into uint64_t.
    FILE* stream = fopen("i.enc","r");
    FILE* out = fopen("BFSession.txt","w");
        if(stream == NULL) {
            printf("Error. stream from fopen is null.\n");
        }else{
         //file is open.
            while(ftell(stream) != -1L){
                fseek(stream, 0, SEEK_END);
                long filesize = (long)ftell(stream);
                // %8 because if its not encrypted, it should only be mod 8 by chance.
                if(filesize%8 == 0  && (filesize -128) > 0){
                    char* buffer = new char[129];
                    string buff;
                    int check = fread(buffer, sizeof(char), 128, stream);
                    if(check == 128){
                        int Elength = 0;
                        uint64_t* packet;
                        packet = stringtoHex(buffer, &Elength);
                        memset(buffer, 0, sizeof(uint64_t));
                        for(int i = 0; i < Elength; i++) {
                            BF.Decrypt(&packet[i], sizeof(packet[i]));
                        }
                        buff = hextoString(packet,Elength);
                        fwrite(buff.c_str(),buff.length(),sizeof(buff),out);
                        filesize = filesize - 128;
                    }else{
                    //this only happens at end of file.
                        filesize = 128 - filesize;
                        int Elength = 0;
                        uint64_t* packet;
                        packet = stringtoHex(buffer, &Elength);
                        memset(buffer, 0, sizeof(uint64_t));
                        for(int i = 0; i < Elength; i++) {
                            BF.Decrypt(&packet[i], sizeof(packet[i]));
                        }
                        buff = hextoString(packet,Elength);
                        fwrite(buff.c_str(),buff.length(),sizeof(buff),out);
                        filesize = 0;
                        delete[] buffer;
                        cout << "Thank you for using BFSession." << endl;
                        return 0;
                    }
                    
                }
            }
        }
	return 0;
}

void eD(){
    string x;
    string out;
    int xl;
    cin >> x;
    cout << x << " Input.\n";
    uint64_t* hex = stringtoHex(x,&xl);
    cout << hex << " Encrypted.\n";
    out = hextoString(hex,xl);
    cout << out << " Decrypted.\n";
}


int main() {
	string input;
	cout << "What do you want to emulate?\nKDC, IDA, or IDB? ";
    cin >> input;
	cin.ignore();
	input = stringToUpper(input);	
    
	if (input.compare("KDC") == 0) {
		cout << "Starting session as KDC" << endl;
		KDC();
	} else if (input.compare("IDA") == 0) {
		cout << "Starting session as IDA" << endl;
		IDA();
	} else if (input.compare("IDB") == 0) {
		cout << "Starting session as IDB" << endl;
		IDB();
	} else {
		cout << input << " is not a valid input. Please enter the correct input. Goodbye." << endl;
        eD();
	}
	return 0;
}



