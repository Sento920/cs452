#include "blowfish.h"

using std::string;

typedef struct Flags {
	int hasH; //boolean --help screen flag
	int hasL; //boolean --listening flag
	int hasV; //boolean --verbose flag (extra info)
	int hasM; //boolean --server takes in offset and numberOfBits --extra credit
	int hasN; //boolean --has specific number of bits to send
	int hasO; //boolena --has a specific offsete to use from the benning of the file
	int port;
	long offset;
	long numberOfBits;
	long fileSize;
	string serverName;
	string fileName;
} Options;

//declared in uwecSocket
int setupServerSocket(int portno); //like new serversocket in java
int callServer(string host, int portno); //like new socket in java
int serverSocketAccept(int serverSocket); //like ss.accept() in java
void writeLong(long x, int socket); //write an int over the given socket
long readLong(int socket); //Read an int from the given socket
void writeLongInt(unsigned long int x, int socket);
unsigned long int readLongInt(int socket);
char* readByte(int socket); //read a byte from the given socket
void writeByte(char* x, int socket); //write a byte from the given socket
int shutdownSocket(int socket, int flags); //shut down socket after use
void writeInt(int x, int socket);
int readInt(int socket);
void writeString(string x,int socket);
string readString(int socket);


//BFSession
class BFSession {
	public:
	int main();
	string stringToUpper(string s);
	//char* stringToHex(char* str);
	int KDC();
	int IDA();
	int IDB();
    uint64_t* stringtoHex(string stringX);
    string hextoString(uint64_t hexVal[], int* out);
    void eD();
};
